import Taro, { Component, requirePlugin } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import PropTypes from 'prop-types'
import './foodList.scss'

export default class FoodList extends Component {
  constructor() {
    super(...arguments)
    this.state = {

    }
  }

  static propTypes = {
    currentList: PropTypes.array
  }

  static defaultProps = {
    currentList: []
  }

  changeTab(value) {
    this.setState({
      current: value
    })
  }

  render() {
    let { currentList, selectCate } = this.props

    return (
     <View className="foodList">
       <Text className="title">{selectCate ? selectCate.name : ''}</Text>
       <View className="foodList-list">
         {
           currentList.map((item, index) => {
             return (
               <View className="foodList-item" key={'key'+index}>
                 <Image className="foodList-item__img" src={require(`../../../assets/img/food${item.imgId}.jpg`)} />
                 <View className="foodList-info">
                  <Text>{item.title}</Text>
                  <Text>月售：<Text className="red">{item.sole}</Text></Text>
                  <Text className="price">￥{item.price}</Text>
                 </View>
               </View>
             )
           })
         }
       </View>
     </View>
    )
  }
}
