import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtTabs, AtTabsPane } from 'taro-ui'
import './cate.scss'

export default class Cate extends Component {
  constructor() {
    super(...arguments)
    this.state = {
      category: [
        { name: '专场', id: 1 },
        { name: '热销', id: 2 },
        { name: '折扣', id: 3 },
        { name: '主食', id: 4 },
        { name: '热炒', id: 5 },
        { name: '凉菜', id: 6 },
        { name: '特色乱炖', id: 7 }
      ],
      selectCate: null
    }
  }

  clickHandle(item) {
    let { selectCate, onChangeCate } = this.props

    if (selectCate && selectCate.id != item.id) {
      this.setState(
        {
          selectCate: item
        },
        () => {
          onChangeCate && onChangeCate(selectCate)
        }
      )
    } else if (!selectCate) {
      this.setState(
        {
          selectCate: item
        },
        () => {
          onChangeCate && onChangeCate(item)
        }
      )
    }
  }


  render() {
    let { selectCate, category } = this.state
    return (
      <View className='cate'>
        {category.map((item, index) => {
          return (
            <Text className={'cate-item' + (selectCate&&selectCate.id == item.id ? ' active' : '')}
              key={index}
              onClick={this.clickHandle.bind(this, item)}
              >
              {item.name}
            </Text>
          )
        })}
      </View>
    )
  }
}
