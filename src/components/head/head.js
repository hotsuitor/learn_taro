import Taro, { Component, requirePlugin } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import Top from './top'
import Activity from '../../components/head/activity'

import './head.scss'

export default class Head extends Component {
  constructor() {
    super(...arguments)
    this.state = {
      store: {
        title: '卧龙居',
        desc: '卧虎藏龙之地',
        tags: ['计谋', '权力', '财富', 'female']
      }
    }
  }
  render() {
    let { store } = this.state
    return (
      <View className="head">
        <Top />
        <Image className='head-bg' src={require('../../assets/img/back.jpg')} />
        <View className='store'>
          <Image className='store-img' src={require('../../assets/img/back.jpg')} />
          <View className='store-info'>
            <Text className='text title'>{store.title}</Text>
            <Text className='text desc'>{store.desc}</Text>
            <View className='text tags'>
              {store.tags.map((item, index) => (
                <Text className='tag' key={index}>
                  {item}
                </Text>
              ))}
            </View>
          </View>
        </View>
        <Activity />
      </View>
    )
  }
}
