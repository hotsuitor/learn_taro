import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './top.scss'

const imgLeft = require('../../assets/img/left.png')

export default class Top extends Component {
  constructor() {
    super(...arguments)
  }

  render() {
    return (
      <View className='top'>
        <View className='left'>
          <Image className='img' src={imgLeft} />
        </View>
        <View className='right'>
          <Image className='img' src={require('../../assets/img/search.png')} />
          <Image
            className='img'
            src={require('../../assets/img/colletion.png')}
          />
          <Image className='img' src={require('../../assets/img/tuan.png')} />
          <Image className='img' src={require('../../assets/img/dian.png')} />
        </View>
      </View>
    )
  }
}
