import Taro, { Component } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
import './activity.scss'

export default class Activity extends Component {
  constructor() {
    super(...arguments)
    this.state = {
      activity: [
        {
          type: 'cut',
          condition: [
            {
              total: 48,
              cut: 5
            },
            {
              total: 58,
              cut: 10
            },
            {
              total: 100,
              cut: 30
            }
          ]
        }
      ]
    }
  }

  getTextByType(type) {
    switch (type) {
      case 'cut':
        return '减'
        break
      default:
        return '减'
        break
    }
  }

  getLine(arr) {
    if (Object.prototype.toString.call(arr) !== '[object Array]') {
      return
    }
    return arr.map(item => `满${item.total}减${item.cut}`).join('；')
  }

  render() {
    let {
      activity,
      activity: [firstItem]
    } = this.state
    return (
      <View className='activity'>
        <Text className='type'>{this.getTextByType(firstItem.type)}</Text>
        <Text className='desc'>{this.getLine(firstItem.condition)}</Text>
        <Text className='length'>{activity.length}个活动</Text>
      </View>
    )
  }
}
